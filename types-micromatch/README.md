# Installation
> `npm install --save @types/micromatch`

# Summary
This package contains type definitions for micromatch (https://github.com/micromatch/micromatch).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/micromatch.

### Additional Details
 * Last updated: Sat, 29 Jun 2024 12:43:08 GMT
 * Dependencies: [@types/braces](https://npmjs.com/package/@types/braces)

# Credits
These definitions were written by [vemoo](https://github.com/vemoo).
